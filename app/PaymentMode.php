<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model
{
    //A Payment Mode belongs to Order (many-to-one)
    public function orders()
    {
        return $this->belongsTo('App\Order');
    }
}
