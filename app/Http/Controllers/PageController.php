<?php

namespace App\Http\Controllers;
use App\Product;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //index method - returns welcome.blade.php with all products
    public function welcome () {
    	$products = Product::orderBy("stock", "desc")->paginate(3);
    	return view('welcome')->with('products', $products);
    }

    public function cart () {
    	return view("pages.cart");
    }

    public function checkout () {
    	return view("pages.checkout");
    }

    public function confirmation () {
    	return view("pages.confirmation");
    }
}
