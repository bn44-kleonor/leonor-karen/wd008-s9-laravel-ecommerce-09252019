//delete product

document.addEventListener("click", function(e){
	//console.log(e.target.classList.contains("btn-delete-product"));

	if(e.target.classList.contains("btn-delete-product"))
	{
		//alert('hello');

		let button = e.target;
		
		//get product_id
		let product_id = button.dataset.id;
		// alert(product_id);


		// get product name
		let product_name = button.dataset.name;
		//put product_name inside span
		let modal_text = document.querySelector("#modal_product_name");
		modal_text.innerHTML = product_name;
		

		//get the modal's form
		let modal_form = document.querySelector("#delete_modal_form");
		//set action attribute to modal form
		modal_form.setAttribute("action", `/products/${product_id}`);

	}
})
